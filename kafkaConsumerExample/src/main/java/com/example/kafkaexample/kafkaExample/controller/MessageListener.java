package com.example.kafkaexample.kafkaExample.controller;

import com.example.kafkaexample.kafkaExample.model.Greeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.concurrent.CountDownLatch;

public class MessageListener {

    public CountDownLatch latch = new CountDownLatch(3);

    public static int count = 0;

    public CountDownLatch partitionLatch = new CountDownLatch(2);

    public CountDownLatch filterLatch = new CountDownLatch(2);

    public CountDownLatch greetingLatch = new CountDownLatch(1);

    @Autowired
    MessageProducer messageProducer = new MessageProducer();

    @KafkaListener(topics = "${message.topic.name}", containerFactory = "concurrentKafkaListenerContainerFactory")
    public void listenGroupTestConsumer(String message) {
        System.out.println("Received Messasge in group 'testConsumer': " + message);
        if(!message.isEmpty() && count==0) {
            processMessageAndPost(message);
            count++;
        }
        latch.countDown();
    }

    @KafkaListener(topics = "${message.topic.name}", containerFactory = "secondKafkaListenerContainerFactory")
    public void listenGroupSecondConsumer(String message) {
        System.out.println("Received Messasge in group 'secondConsumer': " + message);
        latch.countDown();
    }

    @KafkaListener(topics = "${message.topic.name}", containerFactory = "headersKafkaListenerContainerFactory")
    public void listenWithHeaders(@Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        System.out.println("Received Messasge: " + message + " from partition: " + partition);
        latch.countDown();
    }

    @KafkaListener(topicPartitions = @TopicPartition(topic = "${partitioned.topic.name}", partitions = { "0", "3" }))
    public void listenToParition(@Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        System.out.println("Received Message: " + message + " from partition: " + partition);
        this.partitionLatch.countDown();
    }

    @KafkaListener(topics = "${filtered.topic.name}", containerFactory = "filterKafkaListenerContainerFactory")
    public void listenWithFilter(String message) {
        System.out.println("Recieved Message in filtered listener: " + message);
        this.filterLatch.countDown();
    }

    @KafkaListener(topics = "${greeting.topic.name}", containerFactory = "greetingKafkaListenerContainerFactory")
    public void greetingListener(Greeting greeting) {
        System.out.println("Recieved greeting message: " + greeting);
        this.greetingLatch.countDown();
    }

    public void processMessageAndPost(Object object){
        messageProducer.sendGreetingMessage(new Greeting(object.toString(), " World!"));
    }
}
