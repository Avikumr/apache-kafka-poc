package com.example.kafkaexample.kafkaExample;

import com.example.kafkaexample.kafkaExample.controller.MessageProducer;
import com.example.kafkaexample.kafkaExample.model.Greeting;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class KafkaExampleApplication {

	public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(KafkaExampleApplication.class, args);

        MessageProducer producer = context.getBean(MessageProducer.class);

        /*
         * Sending a Hello World message to topic 'testTopic'.
         * Must be recieved by both listeners with group foo
         * and bar with containerFactory fooKafkaListenerContainerFactory
         * and barKafkaListenerContainerFactory respectively.
         * It will also be recieved by the listener with
         * headersKafkaListenerContainerFactory as container factory
         */
       // producer.sendMessage("Hello, World!");

        /*
         * Sending message to a topic with 5 partition,
         * each message to a different partition. But as per
         * listener configuration, only the messages from
         * partition 0 and 3 will be consumed.
         */
//        for (int i = 0; i < 5; i++) {
//            producer.sendMessageToPartion("Hello To Partioned Topic!", i);
//        }

        /*
         * Sending message to 'filtered' topic. As per listener
         * configuration,  all messages with char sequence
         * 'World' will be discarded.
         */
        producer.sendMessageToFiltered("Hello Kafka!");
        producer.sendMessageToFiltered("Hello World!");

        /*
         * Sending message to 'greeting' topic. This will send
         * and recieved a java object with the help of
         * greetingKafkaListenerContainerFactory.
         */
        producer.sendGreetingMessage(new Greeting("Greetings", "World!"));

	}

	@Bean
	public MessageProducer messageProducer() {
		return new MessageProducer();
	}
}
